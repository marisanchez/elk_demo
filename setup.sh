#!/usr/bin/env bash

export DEBIAN_FRONTEND="noninteractive"


# update apt
sudo apt-get update
sudo apt-get DEBIAN_FRONTEND=noninteractive upgrade -y

# install Java 8
sudo apt install -y openjdk-8-jdk
java -version


### Elascticsearch installation

# install Elascticsearch Key
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
# install Elascticsearch package
sudo apt-get update
echo "[*] Installing Elasticsearch package ..."
sudo apt-get install -y elasticsearch
echo "[+] Success Elasticsearch installation"
# copy elasticsearch config
cp /vagrant/elk_configs/elasticsearch/elasticsearch.yml /etc/elasticsearch/
# starting elasticsearch service
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable elasticsearch.service
echo "[*] Elasticsearch is enabled"
sudo /bin/systemctl start elasticsearch.service
echo "[*] Elasticsearch is active"


### Logstash installation

# install Logstash
echo "[*] Installing Logstash package ..."
sudo apt-get install -y logstash
echo "[*] Success logstash installation"
# copy over logstash config
cp -R /vagrant/elk_configs/logstash/* /etc/logstash/conf.d/
# starting logstash service
sudo systemctl enable logstash.service
echo "[*] Logstash is enabled"
sudo systemctl start logstash.service
echo "[*] Logstash is active"


### Logstash installation

# Install Kibana
echo "[*] Installing Kibana package..."
sudo apt-get install -y kibana
echo "[*] Success Kibana package"
# copy over kibana config
cp /vagrant/elk_configs/kibana/kibana.yml /etc/kibana/
# starting kibana service
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable kibana.service
echo "[*] Kibana is enabled"
sudo systemctl start kibana.service
echo "[*] Kibana is active"


### Filebeat installation

# Install filebeat
echo "[*] Installing filebeat package..."
sudo apt-get install -y filebeat
echo "[*] Success filebeat package"
# copy over filebeat config
cp /vagrant/elk_configs/filebeat/filebeat.yml /etc/filebeat/
# starting filebeat service
sudo filebeat modules enable apache2
sudo filebeat modules enable mysql
sudo filebeat modules enable logstash

sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable filebeat.service
echo "[*] filebeat is enabled"
sudo systemctl start filebeat.service
echo "[*] filebeat is active"
